import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import {FormsModule} from '@angular/forms';

import { CardComponent } from './card/card.component';
import { JoueurComponent } from './joueur/joueur.component';
import { SalleComponent } from './salle/salle.component';


@NgModule({
  declarations: [
    AppComponent,
    CardComponent,
    JoueurComponent,
    SalleComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

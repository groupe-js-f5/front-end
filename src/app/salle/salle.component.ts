import { Component } from '@angular/core';
import { Joueur } from '../joueur';
import { Card } from '../card'
@Component({
  selector: 'app-salle',
  templateUrl: './salle.component.html',
  styleUrls: ['./salle.component.css']
})
export class SalleComponent {
  isDragging: boolean = false;
  playerId: number | undefined;
  cardId: string | undefined;

  onDragOver(event: DragEvent) {
    event.preventDefault();
    console.log("DragOver event fired");
  }

  onCardDropped(event: DragEvent) {
    event.preventDefault();
    console.log("CardDropped event fired");

    // Récupérez l'ID de la carte depuis les données de glisser-déposer
    const cardId = event.dataTransfer?.getData('cardId');

    if (this.playerId && cardId) {
      this.sendMessageToBackend(this.playerId, cardId);
    }
  }

  sendMessageToBackend(playerId: number, cardId: string) {
    // Envoyez le message au backend
    // Vous devez implémenter cette fonction pour envoyer les données au backend
    console.log("Sending message to backend - Player ID:", playerId, "Card ID:", cardId);
  }
}

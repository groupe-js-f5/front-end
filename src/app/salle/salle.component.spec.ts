import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SalleComponent } from '../salle/salle.component';

describe('SalleComponent', () => {
  let component: SalleComponent;
  let fixture: ComponentFixture<SalleComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SalleComponent]
    });
    fixture = TestBed.createComponent(SalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should set playerId and cardId when a card is dropped', () => {
    const mockEvent = new DragEvent('drop', { dataTransfer: new DataTransfer() });

    // Simulez l'ID du joueur reçu du backend
    const playerIdFromBackend = 1;

    // Simulez l'ID de la carte glissée
    const cardId = '101';

    component.playerId = playerIdFromBackend;
    component.onCardDropped(mockEvent);

    // Assert that playerId and cardId are set correctly
    expect(component.playerId).toBe(playerIdFromBackend);
    expect(component.cardId).toBe(cardId);
  });

  it('should not set playerId and cardId if no dataTransfer is available', () => {
    const mockEvent = new DragEvent('drop');

    // Simulez l'ID du joueur reçu du backend
    const playerIdFromBackend = 1;

    // Simulez l'ID de la carte glissée
    const cardId = '101';

    component.playerId = playerIdFromBackend;
    component.onCardDropped(mockEvent);

    // Assert that playerId and cardId are not set
    expect(component.playerId).toBe(playerIdFromBackend);
    expect(component.cardId).toBeUndefined();
  });
});

import { Component } from '@angular/core';
import { Card } from './card';
import { Joueur } from './joueur';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'TP-Fil-Rouge-Starter-Front';
  public list: Card[] = [];
  public joueurs: Joueur[] = [];
  public cardsToShow: Card[] = [];

  constructor() {
    this.generateCards();
    this.generateJoueurs();
    this.showInitialCards();
  }

  generateCards() {
    const listeDeNombreDeCartes: number[] = [1, 2, 3, 4, 5, 6, 7, 10, 11, 12];
    const listeDetypeDeCartes: string[] = ["épées", "ors", "coupes", "bâtons"];

    for (const id of listeDeNombreDeCartes) {
      for (const name of listeDetypeDeCartes) {
        const carte: Card = {
          name: name,
          id: id.toString(),
          img: `/assets/tst.webp`,
        };
        this.list.push(carte);
      }
    }
  }

  generateJoueurs() {
    this.joueurs = [
      {
        username: "joueur1",
        Id: "1",
      },
      {
        username: "joueur2",
        Id: "2",
      },
      {
        username: "joueur3",
        Id: "3",
      },
      {
        username: "joueur4",
        Id: "4",
      },
    ];
  }

  showInitialCards() {
    const numberOfCardsToShow = 4;
    this.cardsToShow = this.list.slice(0, numberOfCardsToShow);
  }
}

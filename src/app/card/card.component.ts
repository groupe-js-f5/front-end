import { Component, Input, OnInit } from '@angular/core';
import {Card} from '../card';


@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  @Input() 
  card: Card | null = null;
  selected: boolean = false;
  
  constructor(){}

  ngOnInit(): void {
  }

  startDrag(event: MouseEvent) {
    const card = event.target as HTMLElement;
    card.style.position = 'absolute';
  
    let isDragging = false;
    const shiftX = event.clientX ;
    const shiftY = event.clientY ;
  
    card.style.zIndex = '1000';
  
    const moveAt = (pageX: number, pageY: number) => {
      card.style.left = pageX - shiftX + 'px';
      card.style.top = pageY - shiftY + 'px';
    };
  
    const onMouseMove = (moveEvent: MouseEvent) => {
      if (isDragging) {
        const pageX = moveEvent.pageX;
        const pageY = moveEvent.pageY;
        moveAt(pageX, pageY);
      }
    };
  
    const onMouseUp = () => {
      if (isDragging) {
        document.removeEventListener('mousemove', onMouseMove);
        isDragging = false;
      }
    };
  
    const onMouseDown = (downEvent: MouseEvent) => {
      isDragging = true;
      const pageX = downEvent.clientX;
      const pageY = downEvent.clientY;
      moveAt(pageX, pageY);
  
      document.addEventListener('mousemove', onMouseMove);
      document.addEventListener('mouseup', onMouseUp);
    };
  
    card.addEventListener('mousedown', onMouseDown);
    card.ondragstart = () => false;
  }
  
}
  


